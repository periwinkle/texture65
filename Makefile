BUILDTYPE ?= debug
TARGET := t65

BUILDDIR := build
INCDIR := include
SRCDIR := src
BINDIR := $(BUILDDIR)/$(BUILDTYPE)
OBJDIR := $(BINDIR)/$(SRCDIR)

SRC := $(wildcard $(SRCDIR)/*.c)
OBJ := $(subst $(SRCDIR),$(OBJDIR),$(SRC:.c=.c.o))
INC := $(wildcard $(INCDIR)/*.h)
DEP := $(OBJ:.o=.d)
OUT := $(BINDIR)/$(TARGET)

CPPFLAGS := -I$(INCDIR) -MMD -MP
CFLAGS := -std=c99 -Wall -Wextra -Wpedantic -Werror
LDFLAGS := -lSDL2 -lm -lGL -lGLEW

ifeq ($(BUILDTYPE),debug)
CFLAGS += -g -Og
else ifeq ($(BUILDTYPE),release)
CFLAGS += -O3
else
$(error BUILDTYPE must be one of `debug' or `release')
endif

all: prep $(OUT)

prep:
	mkdir -p $(OBJDIR)

$(OUT): $(OBJ)
	$(CC) $(OBJ) -o $@ $(LDFLAGS)

-include $(DEP)

$(OBJDIR)/%.c.o: $(SRCDIR)/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	rm -r $(BUILDDIR)

run: all
	$(OUT)

.PHONY: all prep clean run
