#ifndef TEX_VIEWER_H_
#define TEX_VIEWER_H_

#include <stddef.h>
#include <stdint.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

typedef struct {
    struct nk_rect location;
    struct nk_vec2 uv;
    size_t px_size;
    struct nk_image texture;
    char title[6];
} t65_tex_info;

enum nk_widget_layout_states t65_tex_viewer_scrolled(struct nk_context *ctx, t65_tex_info *info);
void t65_tex_viewer(struct nk_context *ctx, t65_tex_info *info);
void t65_tex_viewer_custom(struct nk_context *ctx, t65_tex_info *info, float enc_ratio, size_t base_addr);

#endif // TEX_VIEWER_H_
