#ifndef UI_H_
#define UI_H_

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

#include "img_enc.h"

void t65_font_setup(struct nk_context *ctx);
t65_img_encoding t65_enc_select(struct nk_context *ctx, t65_img_encoding cur, int w);
t65_img_alpha t65_alpha_select(struct nk_context *ctx, t65_img_alpha cur, int w);
size_t t65_tlut_addr_edit(struct nk_context *ctx, char *buf, int w);
nk_flags t65_base_addr_edit(struct nk_context *ctx, char *buf);
void t65_base_addr_increment(struct nk_context *ctx, size_t *addr, nk_flags flag);
char *t65_file_prompt(void);
size_t t65_file_read(char *filename, uint8_t **out_ptr);

#endif // UI_H_
