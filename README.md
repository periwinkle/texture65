# texture65

a texture viewer for n64 object files, developed for my use in majora's mask decomp. supports every texture format that [texture64](https://github.com/queueRAM/Texture64) does, borrowed implementation of decoding heavily from there
also. does not support texture dumping and editing, i won't build it but if somebody does i'll accept prs :)

main virtue is linux support, but it has reportedly built and been run on a mac.

dependencies are opengl >= 3, glew, sdl2, c99-compliant compiler
