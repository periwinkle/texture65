#include "img_enc.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/gl.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

#include "macros.h"

const char *encs[] = { "1bpp", "I4", "IA4", "CI4", "I8", "IA8", "CI8", "IA16", "RGBA16", "RGBA32" };
const char *alphas[] = { "Intensity", "Binary", "Full" };

static struct nk_color make_color(uint8_t *data, uint8_t *tlut, uint8_t offset, t65_img_encoding enc, t65_img_alpha a) {
    switch (enc) {
        case OBPP:
            return nk_rgba(0, 0, 0, (data[0] >> (7 - offset)) ? 255 : 0);

        case I4: {
            uint8_t intensity = ((data[0] >> (4 * (1 - offset))) & 0xF) * 17;
            uint8_t alpha = 255;
            switch (a) {
                case INTENSITY:
                    alpha = intensity;
                    break;
                case BINARY:
                    alpha = intensity ? 255 : 0;
                    break;
                case FULL:
                    alpha = 255;
                    break;
            }
            return nk_rgba(intensity, intensity, intensity, alpha);
        }

        case IA4: {
            uint8_t val = ((data[0] >> (4 * (1 - offset))) & 0xF);
            uint8_t intensity = ((val > 1) / 7) * 255;
            uint8_t alpha = (val & 1) ? 255 : 0;
            return nk_rgba(intensity, intensity, intensity, alpha);
        }

        case I8: {
            uint8_t alpha = 255;
            switch (a) {
                case INTENSITY:
                    alpha = data[0];
                    break;
                case BINARY:
                    alpha = data[0] ? 255 : 0;
                    break;
                case FULL:
                    alpha = 255;
                    break;
            }
            return nk_rgba(data[0], data[0], data[0], alpha);
        }

        case IA8: {
            uint8_t intensity = (data[0] >> 4) * 17;
            uint8_t alpha = (data[0] & 0xF) * 17;
            return nk_rgba(intensity, intensity, intensity, alpha);
        }

        case CI4: {
            size_t tlut_idx = (data[0] >> (4 * (1 - offset))) * 2;
            uint8_t r = (tlut[tlut_idx] >> 3) * 255 / 31;
            uint8_t g = (((tlut[tlut_idx] & 7) << 2) | (tlut[tlut_idx + 1] >> 6)) * 255 / 31;
            uint8_t b = ((tlut[tlut_idx + 1] & 0x3E) >> 1) * 255 / 31;
            uint8_t a = (tlut[tlut_idx + 1] & 1) ? 255 : 0;
            return nk_rgba(r, g, b, a);
            break;
        }
        case CI8: {
            size_t tlut_idx = data[0] * 2;
            uint8_t r = (tlut[tlut_idx] >> 3) * 255 / 31;
            uint8_t g = (((tlut[tlut_idx] & 7) << 2) | (tlut[tlut_idx + 1] >> 6)) * 255 / 31;
            uint8_t b = ((tlut[tlut_idx + 1] & 0x3E) >> 1) * 255 / 31;
            uint8_t a = (tlut[tlut_idx + 1] & 1) ? 255 : 0;
            return nk_rgba(r, g, b, a);
            break;
        }

        case IA16:
            return nk_rgba(data[0], data[0], data[0], data[1]);

        case RGBA16: {
            uint8_t r = (data[0] >> 3) * 255 / 31;
            uint8_t g = (((data[0] & 7) << 2) | (data[1] >> 6)) * 255 / 31;
            uint8_t b = ((data[1] & 0x3E) >> 1) * 255 / 31;
            uint8_t a = ((data[1] & 1) ? 255 : 0);
            return nk_rgba(r, g, b, a);
        }

        case RGBA32:
            return nk_rgba(data[0], data[1], data[2], data[3]);
    }
    return nk_rgba(0, 0, 0, 0);
}

struct nk_color *t65_img_decode(uint8_t *data, size_t data_len, uint8_t *tlut, t65_img_encoding enc,
                                t65_img_alpha alpha, size_t *out_len) {
    float ratio = t65_enc_ratio_get(enc);
    size_t decoded_len = data_len / ratio;
    size_t offset_calc = 0;

    if (enc == OBPP) {
        offset_calc = 7;
    } else if (enc == I4 || enc == IA4 || enc == CI4) {
        offset_calc = 1;
    }

    struct nk_color *decoded = calloc(CLAMP_MIN(decoded_len, 64 * 64), sizeof(struct nk_color));
    if (decoded == NULL)
        return NULL;

    for (size_t i = 0; i < decoded_len; i++) {
        uint8_t offset = i & offset_calc;
        decoded[i] = make_color(&data[(size_t)(ratio * i)], tlut, offset, enc, alpha);
    }
    *out_len = decoded_len;
    return decoded;
}

void t65_gen_texture(GLuint texture, struct nk_color *data, struct nk_vec2 uv) {
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, uv.x, uv.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

float t65_enc_ratio_get(t65_img_encoding enc) {
    switch (enc) {
        case OBPP:
            return 0.125;
        case I4:
        case IA4:
        case CI4:
            return 0.5;
        case I8:
        case IA8:
        case CI8:
            return 1;
        case IA16:
        case RGBA16:
            return 2;
        case RGBA32:
            return 4;
    }
    return 1;
}
