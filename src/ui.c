#include "ui.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"
#include "nuklear_sdl_gl3.h"
#include "tinyfiledialogs.h"

#include "img_enc.h"
#include "macros.h"

void t65_font_setup(struct nk_context *ctx) {
    struct nk_font_atlas *atlas;
    nk_sdl_font_stash_begin(&atlas);
    struct nk_font *def = nk_font_atlas_add_default(atlas, 13, 0);
    nk_sdl_font_stash_end();
    nk_style_set_font(ctx, &def->handle);
}

t65_img_encoding t65_enc_select(struct nk_context *ctx, t65_img_encoding cur, int w) {
    nk_layout_space_push(ctx, nk_rect(w - 222, 0, 200, 25));
    nk_label(ctx, "Image Encoding", NK_TEXT_RIGHT);
    nk_layout_space_push(ctx, nk_rect(w - 222, 25, 200, 25));
    t65_img_encoding selected_enc = nk_combo(ctx, encs, ARRLEN(encs), cur, 25, nk_vec2(200, 200));
    return selected_enc;
}

t65_img_alpha t65_alpha_select(struct nk_context *ctx, t65_img_alpha cur, int w) {
    nk_layout_space_push(ctx, nk_rect(w - 222, 50, 200, 25));
    nk_label(ctx, "Alpha", NK_TEXT_RIGHT);
    nk_layout_space_push(ctx, nk_rect(w - 222, 75, 200, 25));
    t65_img_alpha selected_alpha = nk_combo(ctx, alphas, ARRLEN(alphas), cur, 25, nk_vec2(200, 200));
    return selected_alpha;
}

size_t t65_tlut_addr_edit(struct nk_context *ctx, char *buf, int w) {
    nk_layout_space_push(ctx, nk_rect(w - 222, 50, 200, 25));
    nk_label(ctx, "Palette Address", NK_TEXT_RIGHT);
    nk_layout_space_push(ctx, nk_rect(w - 222, 75, 200, 25));
    nk_flags flag = nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, buf, 64, nk_filter_hex);
    size_t tlut_addr = strtoull(buf, NULL, 16);
    if (flag == NK_EDIT_ACTIVE) {
        tlut_addr +=
            nk_input_is_key_pressed(&ctx->input, NK_KEY_UP) - nk_input_is_key_pressed(&ctx->input, NK_KEY_DOWN);
        snprintf(buf, 64, "%zx", tlut_addr);
    }
    return tlut_addr;
}

nk_flags t65_base_addr_edit(struct nk_context *ctx, char *buf) {
    nk_layout_space_push(ctx, nk_rect(80, 0, 60, 25));
    nk_label(ctx, "Base Offset:", NK_TEXT_RIGHT);
    nk_layout_space_push(ctx, nk_rect(190, 0, 60, 25));
    return nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, buf, 64, nk_filter_hex);
}

void t65_base_addr_increment(struct nk_context *ctx, size_t *addr, nk_flags flag) {
    struct nk_vec2 old_padding = ctx->style.button.padding;
    ctx->style.button.padding = nk_vec2(0, 0);
    nk_layout_space_push(ctx, nk_rect(170, 0, 20, 12));
    if (nk_button_symbol(ctx, NK_SYMBOL_TRIANGLE_UP)) {
        (*addr)++;
    } else if (nk_layout_space_push(ctx, nk_rect(170, 13, 20, 12)),
               *addr > 0 && nk_button_symbol(ctx, NK_SYMBOL_TRIANGLE_DOWN)) {
        (*addr)--;
    }
    if (flag == NK_EDIT_ACTIVE) {
        (*addr) += nk_input_is_key_pressed(&ctx->input, NK_KEY_UP) - nk_input_is_key_pressed(&ctx->input, NK_KEY_DOWN);
    }
    ctx->style.button.padding = old_padding;
}

char *t65_file_prompt(void) {
    return tinyfd_openFileDialog("Open File", NULL, 0, NULL, NULL, 0);
}

size_t t65_file_read(char *filename, uint8_t **out_ptr) {
    FILE *f = fopen(filename, "rb");
    if (f != NULL) {
        fseek(f, 0, SEEK_END);
        size_t raw_len = ftell(f);
        fseek(f, 0, SEEK_SET);
        uint8_t *raw = malloc(raw_len);
        if (raw == NULL || fread(raw, 1, raw_len, f) != raw_len) {
            return 0;
        }
        fclose(f);
        *out_ptr = raw;
        return raw_len;
    }
    return 0;
}
